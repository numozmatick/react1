import React, {Component} from 'react';
import s from './Description.module.scss'
import MagazineItem from "../../aside/magazinItem/MagazineItem";
import {NavLink} from "react-router-dom";
 let Description = (props) => {

        const magId = props.id;
        let descState = props.state.companies.find(ID => ID.id == magId)

        return (
            <div className={s.wrapper}>
                <NavLink to={`/settings/`}  className={s.settings}><i className="fas fa-cog"></i></NavLink>
                <div className={s.header}>
                    <div className={s.title}>
                        <h3>{descState.nameItem}</h3>
                        <p>{descState.subname}</p>
                    </div>
                    {/*<img src={this.state.img} alt=""/>*/}
                    <div className={s.descr}>
                        {descState.description}
                    </div>
                </div>
                <div className={s.schedule}>
                    <h4>Время работы</h4>
                    <table>
                        <tbody>
                        <tr>
                            <td>Пн-Пт:</td>
                            <td>{descState.schedule.weekdays}</td>
                        </tr>
                        <tr>
                            <td>Суббота:</td>
                            <td>{descState.schedule.saturday}</td>
                        </tr>
                        <tr>
                            <td>Воскресенье:</td>
                            <td>{descState.schedule.sunday}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }

export default Description;
