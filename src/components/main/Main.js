import React, { Component } from 'react';
import s from './Main.module.scss'
import Description from "./description/Description";
import {BrowserRouter, Route} from 'react-router-dom';
import Settings from './settings/Settings'
let Main = (props) => {

      return (
            <div className={s.wrapper}>
                <Route path='/description/:itemId' render={({match:{params:{itemId}}})=><Description id = {itemId} state={props.state} />}/>
                <Route path='/settings/' render={<Settings/>} />
    </div>
    );

}

export default Main;
