import React, {Component} from 'react';
import s from './MagazineItem.module.scss'
import {NavLink, Route} from "react-router-dom";

let MagazineItem = (props) => {
    let itemId = props.state.id;
    return (
        <div className={s.card}>
            <div className={s.about}>
                <h3>
                    {props.state.nameItem}
                </h3>
                <p>
                    {props.state.subname}
                </p>
                <NavLink to={`/description/${itemId}`}>
                    <div className={s.about_btn}> Подробнее о нас</div>
                </NavLink>
                <div className={s.location}>
                    <div className={s.location__address}>{props.state.address}</div>
                    <a href="/" className={s.location__filial}>{props.state.filial} филиалов</a>
                </div>
            </div>
            <img src={props.state.image} alt="картинка магазина"/>
        </div>
    );
}

export default MagazineItem;
