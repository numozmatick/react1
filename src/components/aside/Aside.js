import React, { Component } from 'react';
import MagazineItem from "./magazinItem/MagazineItem";
import s from './Aside.module.scss'
let Aside = (props) =>  {
      let p = props.state.companies;
      let Magazines = p.map(el =>
          <MagazineItem state = {el}/>
      )
      return (
            <div className={s.wrapper}>
                {Magazines}
            </div>
    );
}

export default Aside;
