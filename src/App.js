import React from 'react';
import Aside from './components/aside/Aside'
import Main from "./components/main/Main";
import s from "./App.module.scss"

let App = (props) => {

    return (
        <div className={s.app_wrapper}>
            <header></header>
            <aside><Aside state={props.state}/></aside>
            <main><Main state={props.state}/></main>
            <footer></footer>

        </div>
    );
}

export default App