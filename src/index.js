import React from 'react';
import ReactDOM from 'react-dom'
import store, {subscribe} from "./redux/state";
import {BrowserRouter} from "react-router-dom";
import App from "./App";

export let rerenderEntireTree = (store) => {
    ReactDOM.render(
        <BrowserRouter>
            <App state = {store}/>
        </BrowserRouter>,
        document.getElementById('root') );
}


rerenderEntireTree(store.getState());
subscribe(rerenderEntireTree);